#pragma once
#include "problem.h"

double f0(double t, double* y);

double f1(double t, double* y);

double f2(double t, double* y);

double f3(double t, double* y);

double f4(double t, double* y);

double f5(double t, double* y);

double f6(double t, double* y);

double f7(double t, double* y);

double f8(double t, double* y);

double f9(double t, double* y);

double f10(double t, double* y);

double f11(double t, double* y);

double f12(double t, double* y);

double f13(double t, double* y);

double f14(double t, double* y);

double f15(double t, double* y);

double f16(double t, double* y);

double f17(double t, double* y);

double f18(double t, double* y);

double f19(double t, double* y);

double f20(double t, double* y);

double f21(double t, double* y);

double f22(double t, double* y);

double f23(double t, double* y);

double f24(double t, double* y);

double f25(double t, double* y);

double f26(double t, double* y);

double f27(double t, double* y);

double f28(double t, double* y);

double f29(double t, double* y);

double f30(double t, double* y);

double f31(double t, double* y);

double f32(double t, double* y);

double f33(double t, double* y);

double f34(double t, double* y);

double f35(double t, double* y);

double f36(double t, double* y);

double f37(double t, double* y);

double f38(double t, double* y);

double f39(double t, double* y);

double f40(double t, double* y);

double f41(double t, double* y);

double f42(double t, double* y);

double f43(double t, double* y);

double f44(double t, double* y);

double f45(double t, double* y);

double f46(double t, double* y);

double f47(double t, double* y);

double f48(double t, double* y);

double f49(double t, double* y);

double f50(double t, double* y);

double f51(double t, double* y);

double f52(double t, double* y);

double f53(double t, double* y);

double f54(double t, double* y);

double f55(double t, double* y);

double f56(double t, double* y);

double f57(double t, double* y);

double f58(double t, double* y);

double f59(double t, double* y);

double f60(double t, double* y);

double f61(double t, double* y);

double f62(double t, double* y);

double f63(double t, double* y);

double f64(double t, double* y);

double f65(double t, double* y);

double f66(double t, double* y);

double f67(double t, double* y);

double f68(double t, double* y);

double f69(double t, double* y);

double f70(double t, double* y);

double f71(double t, double* y);

double f72(double t, double* y);

double f73(double t, double* y);

double f74(double t, double* y);

double f75(double t, double* y);

double f76(double t, double* y);

double f77(double t, double* y);

double f78(double t, double* y);

double f79(double t, double* y);

double f80(double t, double* y);

double f81(double t, double* y);

double f82(double t, double* y);

double f83(double t, double* y);

double f84(double t, double* y);

double f85(double t, double* y);

double f86(double t, double* y);

double f87(double t, double* y);

double f88(double t, double* y);

double f89(double t, double* y);

double f90(double t, double* y);

double f91(double t, double* y);

double f92(double t, double* y);

double f93(double t, double* y);

double f94(double t, double* y);

double f95(double t, double* y);

double f96(double t, double* y);

double f97(double t, double* y);

double f98(double t, double* y);

double f99(double t, double* y);

void getInitCond(double* t, double** y0);

rp_func_t* getSystemRightPart();

unsigned int getSystemOrder();
