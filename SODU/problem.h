#pragma once
#include <stdio.h>
#include <Windows.h>

typedef double (*rp_func_t)(double, double*);

typedef struct {
	unsigned int eqNum;
	double startTime;
	rp_func_t* f;
	double* y0;
	double* solution;
	double* timeGrid;
	unsigned int gridLen;
} problem_t;

problem_t* InitProblem(unsigned int eqNum, rp_func_t* rightPart, double startTime, double* y0);

void SolveProblem(problem_t* problem, unsigned int nodesNum, double endTime, int threads_num);

void PrintSolution(problem_t* problem, FILE* desc);

void KillProblem(problem_t* problem);
