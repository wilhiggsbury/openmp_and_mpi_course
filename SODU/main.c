#include "problem.h"
#include "system.h"
#include <stdio.h>
#include <Windows.h>
#include <omp.h>

#pragma warning(disable:4996)

void solutionExp() {
	const int threads_num = 1;
	unsigned int eqNum = getSystemOrder();
	rp_func_t* rightPart = getSystemRightPart();
	double startTime = 0;
	double* initVals = NULL;
	getInitCond(&startTime, &initVals);
	problem_t* problem = InitProblem(eqNum, rightPart, startTime, initVals);
	LARGE_INTEGER timeStamp[2];
	LARGE_INTEGER frequency = { 0 };
	ZeroMemory(timeStamp, sizeof(timeStamp));
	if (!QueryPerformanceFrequency(&frequency)) {
		printf("Performance counter error\n");
		KillProblem(problem);
		return;
	}
	QueryPerformanceCounter(timeStamp);
	SolveProblem(problem, 100, 2, threads_num);
	QueryPerformanceCounter(timeStamp + 1);
	if (!problem->solution) {
		printf("Memory error\n");
		KillProblem(problem);
		return;
	}
	FILE* f_out = fopen("matlab/solution.txt", "w");
	if (!f_out) {
		printf("File creation error\n");
	}
	else {
		PrintSolution(problem, f_out);
		fclose(f_out);
	}
	printf("%-10s | %10s\n\n", "time", "values");
	for (size_t i = 0; i < problem->gridLen; i++) {
		printf("%-10g | ", problem->timeGrid[i]);
		for (size_t j = 0; j < problem->eqNum; j++) {
			printf("%lf ", problem->solution[i * problem->eqNum + j]);
		}
		printf("\n");
	}

	printf("Time elapsed: %g s.", (double)(timeStamp[1].QuadPart - timeStamp[0].QuadPart) / frequency.QuadPart);
	KillProblem(problem);
	free(rightPart);
	free(initVals);
}

void timeExp() {
	int threadArr[] = { 1, 2, 4, 6, 8, 10, 12 };
	double results[ARRAYSIZE(threadArr)] = { 0 };
	int iterNum = 1000;
	unsigned int eqNum = getSystemOrder();
	rp_func_t* rightPart = getSystemRightPart();
	double startTime = 0;
	double* initVals = NULL;
	getInitCond(&startTime, &initVals);
	problem_t* problem = NULL;
	LARGE_INTEGER timeStamp[2];
	LARGE_INTEGER frequency = { 0 };
	LARGE_INTEGER timeSum = { 0 };
	ZeroMemory(timeStamp, sizeof(timeStamp));
	if (!QueryPerformanceFrequency(&frequency)) {
		printf("Performance counter error\n");
		return;
	}
	for (int i = 0; i < ARRAYSIZE(threadArr); i++) {
		timeSum.QuadPart = 0;
		for (int j = 0; j < iterNum; j++) {
			problem = InitProblem(eqNum, rightPart, startTime, initVals);
			QueryPerformanceCounter(timeStamp);
			SolveProblem(problem, 100, 2, threadArr[i]);
			QueryPerformanceCounter(timeStamp + 1);
			timeSum.QuadPart += timeStamp[1].QuadPart - timeStamp[0].QuadPart;
			KillProblem(problem);
		}
		results[i] = (double)(((long double)(timeSum.QuadPart) / frequency.QuadPart) / iterNum);
		printf("Avg. time elapsed with %i threads: %g s.\n", threadArr[i], results[i]);
	}
	FILE* f_out = fopen("matlab/time_elapsed.txt", "w");
	if (!f_out) {
		printf("File creation error\n");
		free(rightPart);
		free(initVals);
		return;
	}
	for (int i = 0; i < ARRAYSIZE(threadArr); i++) {
		fprintf(f_out, "%i", threadArr[i]);
		if (i < ARRAYSIZE(threadArr) - 1) {
			fprintf(f_out, ", ");
		}
	}
	fprintf(f_out, "\n");
	for (int i = 0; i < ARRAYSIZE(threadArr); i++) {
		fprintf(f_out, "%lf", results[i]);
		if (i < ARRAYSIZE(threadArr) - 1) {
			fprintf(f_out, ", ");
		}
	}
	fclose(f_out);
	free(rightPart);
	free(initVals);
}

int main() {
	//solutionExp();
	timeExp();
	return 0;
}
