#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <stdio.h>
#include "problem.h"

problem_t* InitProblem(unsigned int eqNum, rp_func_t* rightPart, double startTime, double* y0) {
	problem_t* result = malloc(sizeof(problem_t));
	size_t rightPartSize = eqNum * sizeof(rightPart);
	size_t y0Size = eqNum * sizeof(double);
	if (!result) {
		return NULL;
	}
	result->eqNum = eqNum;
	result->f = malloc(rightPartSize);
	if (!result->f) {
		free(result);
		return NULL;
	}
	memcpy(result->f, rightPart, rightPartSize);
	result->startTime = startTime;
	result->y0 = malloc(y0Size);
	if (!result->y0) {
		free(result->f);
		free(result);
		return NULL;
	}
	memcpy(result->y0, y0, y0Size);
	result->solution = NULL;
	result->timeGrid = NULL;
	result->gridLen = 0;
	return result;
}

void SolveProblem(problem_t* problem, unsigned int nodesNum, double endTime, int threads_num) {
	double h = (endTime - problem->startTime) / (nodesNum - 1);
	double t = problem->startTime;
	double* k = NULL;
	double* yBuffer1 = NULL;
	double* yBuffer2 = NULL;
	const double kCoeff[] = { 1.0 / 6, 2.0 / 6, 2.0 / 6, 1.0 / 6 };
	size_t curEqIdx = 0;
	size_t prevEqIdx = 0;
	size_t idxK = 0;
	int j;
	int eqNum = (int)problem->eqNum;
	const size_t singleSolSize = eqNum * sizeof(double);
	if (h < 0) {
		return;
	}
	k = malloc(4 * singleSolSize);
	problem->solution = malloc(nodesNum * singleSolSize);
	yBuffer1 = malloc(singleSolSize);
	yBuffer2 = malloc(singleSolSize);
	problem->timeGrid = malloc(nodesNum * sizeof(double));
	if (!problem->solution || !k || !yBuffer1 || !yBuffer2 || !problem->timeGrid) {
		free(k);
		free(yBuffer1);
		free(yBuffer2);
		free(problem->solution);
		free(problem->timeGrid);
		problem->solution = NULL;
		return;
	}
	memcpy(problem->solution, problem->y0, singleSolSize);
	problem->timeGrid[0] = t;
	omp_set_num_threads(threads_num);
#pragma omp parallel 
	{
	for (size_t i = 1; i < nodesNum; i++) {
#pragma omp for private(j)
		for (j = 0; j < eqNum; j++) {
			k[j] = h * problem->f[j](t, problem->solution + prevEqIdx);
			yBuffer1[j] = problem->solution[prevEqIdx + j] + 0.5 * k[j];
		}
		// implicit synchro
#pragma omp for private(j, idxK)
		for (j = 0; j < eqNum; j++) {
			idxK = eqNum + j;
			k[idxK] = h * problem->f[j](t + h / 2, yBuffer1);
			yBuffer2[j] = problem->solution[prevEqIdx + j] + 0.5 * k[idxK];
		}
		// implicit synchro
#pragma omp for private(j, idxK)
		for (j = 0; j < eqNum; j++) {
			idxK = 2 * eqNum + j;
			k[idxK] = h * problem->f[j](t + h / 2, yBuffer2);
			yBuffer1[j] = problem->solution[prevEqIdx + j] + k[idxK];
		}
#pragma omp for private(j, idxK)
		for (j = 0; j < eqNum; j++) {
			idxK = 3 * eqNum + j;
			k[idxK] = h * problem->f[j](t + h, yBuffer1);
		}
#pragma omp for private(j, curEqIdx)
		for (j = 0; j < eqNum; j++) {
			curEqIdx = prevEqIdx + eqNum + j;
			problem->solution[curEqIdx] = problem->solution[prevEqIdx + j] + \
				kCoeff[0] * k[j] + kCoeff[1] * k[j + eqNum] + \
				kCoeff[2] * k[j + 2 * eqNum] + kCoeff[3] * k[j + 3 * eqNum];
			//printf("eq #%i\n", j);
		}
#pragma omp single
		{
			//printf("\n");
			t += h;
			problem->timeGrid[i] = t;
			prevEqIdx += eqNum;
		}
	}
	}
	problem->gridLen = nodesNum;
	free(k);
	free(yBuffer1);
	free(yBuffer2);
}

// first column is the time stamp, next are solution y_1, ..., y_n
void PrintSolution(problem_t* problem, FILE* desc) {
	if (!problem || !problem->solution) {
		return;
	}
	for (size_t i = 0; i < problem->gridLen; i++) {
		fprintf(desc, "%lf", problem->timeGrid[i]);
		for (size_t j = 0; j < problem->eqNum; j++) {
			fprintf(desc, ", %lf", problem->solution[i * problem->eqNum + j]);
		}
		fprintf(desc, "\n");
	}
}

void KillProblem(problem_t* problem) {
	free(problem->y0);
	free(problem->f);
	free(problem->solution);
	free(problem->timeGrid);
	free(problem);
}
