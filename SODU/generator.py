import numpy as np

# X' = A*X + B, C - init cond at 0
def gen_odes_mtx(n, coefs_bounds=(-1.5, 1), round=2, f_free_coefs=True):
    lb, ub = coefs_bounds
    non_trivial_rows = 87
    ones_at_nt_rows = 80
    non_zeroed_t = 90
    idx = np.arange(n)
    A = np.zeros((n, n))
    if f_free_coefs:
        B = np.random.uniform(lb, ub, (n, 2)).round(round) # a*t + b
        B[np.random.choice(idx, n - non_zeroed_t, False), 0] = 0
    else:
        B = np.zeros((n, 2))
    C = np.random.uniform(lb, ub, n).round(round)
    A_idx = np.random.choice(idx, non_trivial_rows, False)
    np.random.shuffle(idx)
    for i in range(n):
        A[i, idx[i]] = 1
    for i in range(non_trivial_rows):
        ones_idx = np.random.choice(idx, ones_at_nt_rows, False)
        A[A_idx[i], ones_idx] = 1
    return A, B, C
        
def get_matlab_code(A, B, C):
    n = A.shape[0]
    output = 'clear all\n\n'
    output += 'syms '
    for i in range(n):
        output += f'x{i}(t) '
    output += '\nA = ['
    for i in range(n):
        output += f'{np.array2string(A[i], max_line_width=1000)}; '
    output += '];\nB = ['
    for i in range(n):
        output += f'{B[i, 0]}*t+{B[i, 1]}; '
    output += '];\nY = ['
    for i in range(n):
        output += f'x{i}; '
    output += '];\nC = Y(0) == '
    output += np.array2string(C, separator=';')
    output += ';\nodes = diff(Y) == A*Y + B\n['
    for i in range(n):
        output += f'x{i}Sol(t), '
    output += '] = dsolve(odes, C);\n'
    for i in range(n):
        output += f'x{i}Sol(t) = simplify(x{i}Sol(t))\n'
    return output
    
def get_c_code(A, B, C, header_name):
    n = A.shape[0]
    output_c = f'#include "{header_name}"\n#include <stdlib.h>\n#include <string.h>\n\n'
    output_h = '#pragma once\n#include "problem.h"\n\n'
    for i in range(n):
        cur_func = f'double f{i}(double t, double* y)'
        output_h += cur_func + ';\n\n'
        cur_func +=  ' {\n'
        return_row = '    return '
        for j in range(n):
            return_row += f'{'+' if A[i, j] >= 0 else ''}{A[i, j]} * y[{j}] '
        return_row += f'+ {B[i, 0]} * t {'+' if B[i, 1] >= 0 else ''}{B[i, 1]};'
        cur_func += return_row + '\n}\n\n'
        output_c += cur_func
    init_cond = 'void getInitCond(double* t, double** y0)'
    output_h += init_cond + ';\n\n'
    init_cond += ' {\n    double arr[] = {'
    init_cond += np.array2string(C, separator=', ')[1:-1]
    init_cond += '};\n    '
    init_cond += '*y0 = malloc(sizeof(arr));\n    '
    init_cond += '*t = 0;\n    memcpy(*y0, arr, sizeof(arr));\n}\n\n'
    output_c += init_cond
    rp_getter = 'rp_func_t* getSystemRightPart()'
    output_h += rp_getter + ';\n\n'
    output_c += rp_getter + ' {\n    rp_func_t funcs[] = {'
    funcs = ', '.join([f'f{i}' for i in range(n)])
    output_c += funcs
    output_c += '};\n    rp_func_t* res = malloc(sizeof(funcs));\n    '
    output_c += 'memcpy(res, funcs, sizeof(funcs));\n    return res;\n}\n\n'
    sys_n_getter = 'unsigned int getSystemOrder()'
    output_h += sys_n_getter + ';\n'
    output_c += sys_n_getter + ' {\n    return ' + str(n) + ';\n}\n'
    return output_h, output_c

if __name__=='__main__':
    seed = 234
    np.random.seed(seed)
    
    n = 100
    A, B, C = gen_odes_mtx(n)
    print(A)
    print(B)
    print(C)
    c_header, c_code = get_c_code(A, B, C, 'system.h')
    with open('system.h', 'w') as file:
        file.write(c_header)
    with open('system.c', 'w') as file:
        file.write(c_code)
    # matlab_script = get_matlab_code(A, B, C)
    # with open('matlab.txt', 'w') as file:
    #     file.write(matlab_script)
