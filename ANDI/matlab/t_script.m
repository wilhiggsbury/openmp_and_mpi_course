clear all
close all

threads = [1, 2, 4, 6, 8, 10, 12];
time_mean = zeros(size(threads));
time_std = zeros(size(threads));
for i=1:size(threads, 2)
    time_data = readmatrix(sprintf('time_%i_mpi.txt', threads(i)));
    time_mean(i) = mean(time_data);
    time_std(i) = std(time_data);
end

time_high = time_mean + time_std;
time_low = time_mean - time_std;

figure()
plot(threads, time_mean, 'b','marker', 's', 'DisplayName', 'T(p)')
hold on
plot(threads, time_high, ':b', 'DisplayName', 'T(p) \pm\sigma(T)')
hold on
plot(threads, time_low, ':b', 'HandleVisibility','off')
grid on
legend()
title('MPI T(p)')
xlim([1, max(threads)])
xlabel('p')
ylabel('Execution time')

figure()
S = time_mean(1) * ones(size(time_mean)) ./ time_mean;
S_low = time_low(1) * ones(size(time_mean)) ./ time_low;
S_high = time_high(1) * ones(size(time_mean)) ./ time_high;
plot(threads, S, 'b', 'marker', 's', 'DisplayName', 'S(p)')
hold on
plot(threads, S_low, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, S_high, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, threads, 'k--', 'DisplayName', 'Perfect case')
grid on
legend('Location', 'northwest')
title('MPI S(p)')
xlabel('p')
ylabel('S(p)')
xlim([min(threads), max(threads)])
ylim([1, max(threads)])

figure()
plot(threads, S ./ threads, 'b', 'marker', 's', 'DisplayName', 'E(p)')
hold on 
plot(threads, S_low ./ threads, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, S_high ./ threads, ':b', 'HandleVisibility', 'off')
hold on
grid on
xlabel('p')
ylabel('E(p)')
xlim([1, max(threads)])
legend()
title('MPI E(p)')

time_mean = zeros(size(threads));
time_std = zeros(size(threads));
for i=1:size(threads, 2)
    time_data = readmatrix(sprintf('time_%i_omp.txt', threads(i)));
    time_mean(i) = mean(time_data);
    time_std(i) = std(time_data);
end

time_high = time_mean + time_std;
time_low = time_mean - time_std;

figure()
plot(threads, time_mean, 'b','marker', 's', 'DisplayName', 'T(p)')
hold on
plot(threads, time_high, ':b', 'DisplayName', 'T(p) \pm\sigma(T)')
hold on
plot(threads, time_low, ':b', 'HandleVisibility','off')
grid on
legend()
xlim([1, max(threads)])
xlabel('p')
ylabel('Execution time')
title('OMP T(p)')

figure()
S = time_mean(1) * ones(size(time_mean)) ./ time_mean;
S_low = time_low(1) * ones(size(time_mean)) ./ time_low;
S_high = time_high(1) * ones(size(time_mean)) ./ time_high;
plot(threads, S, 'b', 'marker', 's', 'DisplayName', 'S(p)')
hold on
plot(threads, S_low, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, S_high, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, threads, 'k--', 'DisplayName', 'Perfect case')
grid on
legend('Location', 'northwest')
xlabel('p')
ylabel('S(p)')
xlim([min(threads), max(threads)])
ylim([1, max(threads)])
title('OMP S(p)')

figure()
plot(threads, S ./ threads, 'b', 'marker', 's', 'DisplayName', 'E(p)')
hold on 
plot(threads, S_low ./ threads, ':b', 'HandleVisibility', 'off')
hold on
plot(threads, S_high ./ threads, ':b', 'HandleVisibility', 'off')
hold on
grid on
xlabel('p')
ylabel('E(p)')
xlim([1, max(threads)])
title('OMP E(p)')
legend()