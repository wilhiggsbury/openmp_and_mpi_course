from PIL import Image
from sys import argv


if __name__=='__main__':
    assert len(argv) == 4
    _, in_path, mode, out_path = argv
    assert mode == 'txt' or mode == 'img'
    if mode == 'txt':
        img = Image.open(in_path)
        img_gs = img.convert('L')
        data = img_gs.getdata()
        with open(out_path, 'wb') as file:
            file.write(bytes(' '.join(map(str, data.size)), 'ASCII'))
            file.write(bytes('\n', 'ASCII'))
            file.write(bytes(data))
    else:
        with open(in_path, 'rb') as file:
            w, h = file.readline().split()
            data = file.read()
        img = Image.frombytes('L', (int(w), int(h)), data)
        img.save(out_path)
        img.show()
