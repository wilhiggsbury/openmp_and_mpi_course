#pragma once

typedef struct {
    double* data_prev;
    double* data_cur;
    double* buf_lb;
    double* buf_ub;
    double* mpi_buffer;
    int proc_id;
    int y_idx_bounds[2];
    int y_n;
    int x_n;
} problem_t;

problem_t* InitProblem(double* v0, int x_len, int y_len, int proc_id, int procs_n);

void SolveProblem(problem_t* problem, int iter_n, double lambda, double K);

void KillProblem(problem_t* problem);
