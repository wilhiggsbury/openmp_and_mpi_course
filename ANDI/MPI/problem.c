#include "problem.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// up and low y bounds according to the index
// for the recipient
#define TAG_UP_BND 16
#define TAG_LOW_BND 32

void get_y_bounds(int* bounds, int cur_proc_id, int procs_n, int y_n) {
    int layer_thick = y_n / procs_n;
    bounds[0] = cur_proc_id * layer_thick;
    bounds[1] = bounds[0] + layer_thick;
    if (cur_proc_id == procs_n - 1) {
        bounds[1] += y_n % procs_n;
    }
}

problem_t* InitProblem(double* v0, int x_len, int y_len, int proc_id, int procs_n) {
    problem_t* problem = calloc(1, sizeof(problem_t));
    int proc_y_n = 0;
    if (!problem) {
        printf("Memory allocation error!\n");
        free(problem);
        return NULL;
    }
    problem->proc_id = proc_id;
    get_y_bounds(problem->y_idx_bounds, proc_id, procs_n, y_len);
    if (problem->y_idx_bounds[0] != 0) {
        problem->buf_lb = malloc(x_len * sizeof(double));
        if (!problem->buf_lb) {
            printf("Memory allocation error!\n");
            free(problem);
            return NULL;
        }
        memcpy(problem->buf_lb, v0 + (problem->y_idx_bounds[0] - 1) * x_len,
               x_len * sizeof(double));
    }
    if (problem->y_idx_bounds[1] != y_len) {
        problem->buf_ub = malloc(x_len * sizeof(double));
        if (!problem->buf_ub) {
            printf("Memory allocation error!\n");
            free(problem->buf_lb);
            free(problem);
            return NULL;
        }
        memcpy(problem->buf_ub, v0 + problem->y_idx_bounds[1] * x_len, x_len * sizeof(double));
    }
    proc_y_n = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    problem->x_n = x_len;
    problem->y_n = y_len;
    int buf_num = 4 * (!!problem->buf_lb + !!problem->buf_ub);
    if (buf_num > 0) {
        problem->mpi_buffer = malloc(buf_num * x_len * sizeof(double) + 200);
        if (!problem->mpi_buffer) {
            printf("Memory allocation error!\n");
            free(problem->buf_lb);
            free(problem->buf_ub);
            free(problem);
            return NULL;
        }
        int rc = MPI_Buffer_attach(problem->mpi_buffer, buf_num * x_len * sizeof(double) + 200);
        if (rc != MPI_SUCCESS) {
            printf("Buffer attaching error! %i\n", rc);
            free(problem->buf_lb);
            free(problem->buf_ub);
            free(problem);
            return NULL;
        }
    }
    problem->data_cur = malloc(x_len * proc_y_n * sizeof(double));
    problem->data_prev = malloc(x_len * proc_y_n * sizeof(double));
    if (!problem->data_cur || !problem->data_prev) {
        free(problem->data_cur);
        free(problem->data_prev);
        free(problem->buf_lb);
        free(problem->buf_ub);
        free(problem);
        return NULL;
    }
    memcpy(problem->data_cur, v0 + problem->y_idx_bounds[0] * x_len, x_len * proc_y_n * sizeof(double));
    return problem;
}

double G(double x, double K) {
    return exp(-pow(fabs(x) / K, 2));
}

void FillInside(double* p, double* c, int n, int m, double lambda, double K) {
    double gn, gs, ge, gw;
    for (int i = 1; i < n - 1; i++) {
        for (int j = 1; j < m - 1; j++) {
            gn = p[(i - 1) * m + j] - p[i * m + j];
            gs = p[(i + 1) * m + j] - p[i * m + j];
            ge = p[i * m + j + 1] - p[i * m + j];
            gw = p[i * m + j - 1] - p[i * m + j];
            c[i * m + j] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge + G(gw, K) * gw;
            c[i * m + j] *= lambda;
            c[i * m + j] += p[i * m + j];
        }
    }
}

void FillBounds(problem_t* problem, double lambda, double K, int f_left, int f_right, int f_low, int f_up) {
    double gn, gs, ge, gw;
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    int m = problem->x_n;
    const int big_shift = (layer_width - 1) * m;

    if (f_left) {
        // left border
        for (int i = 1; i < layer_width - 1; i++) {  // j is for x, i is for y
            gs = problem->data_prev[(i + 1) * m] - problem->data_prev[i * m];
            ge = problem->data_prev[i * m + 1] - problem->data_prev[i * m];
            gn = problem->data_prev[(i - 1) * m + 1] - problem->data_prev[i * m];
            problem->data_cur[i * m] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
            problem->data_cur[i * m] *= lambda;
            problem->data_cur[i * m] += problem->data_prev[i * m];
        }
    }

    if (f_right) {
        // right border
        for (int i = 1; i < layer_width - 1; i++) {  // j is for x, i is for y
            gs = problem->data_prev[(i + 2) * m - 1] - problem->data_prev[(i + 1) * m - 1];
            gw = problem->data_prev[(i + 1) * m - 2] - problem->data_prev[(i + 1) * m - 1];
            gn = problem->data_prev[(i - 1) * m + 1] - problem->data_prev[(i + 1) * m - 1];
            problem->data_cur[(i + 1) * m - 1] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
            problem->data_cur[(i + 1) * m - 1] *= lambda;
            problem->data_cur[(i + 1) * m - 1] += problem->data_prev[(i + 1) * m - 1];
        }
    }

    // lower border
    if (f_low) {
        // case i = 0;
        for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
            gs = problem->data_prev[m + j] - problem->data_prev[j];
            ge = problem->data_prev[j + 1] - problem->data_prev[j];
            gw = problem->data_prev[j - 1] - problem->data_prev[j];
            if (problem->y_idx_bounds[0] > 0) {
                gn = problem->buf_lb[j] - problem->data_prev[j];
            }
            else {
                gn = 0;
            }
            problem->data_cur[j] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge + G(gw, K) * gw;
            problem->data_cur[j] *= lambda;
            problem->data_cur[j] += problem->data_prev[j];
        }
        // j = 0
        gs = problem->data_prev[m] - problem->data_prev[0];
        ge = problem->data_prev[1] - problem->data_prev[0];
        if (problem->y_idx_bounds[0] > 0) {
            gn = problem->buf_lb[0] - problem->data_prev[0];
        }
        else {
            gn = 0;
        }
        problem->data_cur[0] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
        problem->data_cur[0] *= lambda;
        problem->data_cur[0] += problem->data_prev[0];

        // j = m - 1
        gs = problem->data_prev[2 * m - 1] - problem->data_prev[m - 1];
        gw = problem->data_prev[m - 2] - problem->data_prev[m - 1];
        if (problem->y_idx_bounds[0] > 0) {
            gn = problem->buf_lb[m - 1] - problem->data_prev[m - 1];
        }
        else {
            gn = 0;
        }
        problem->data_cur[m - 1] = G(gs, K) * gs + G(gw, K) * gw + G(gn, K) * gn;
        problem->data_cur[m - 1] *= lambda;
        problem->data_cur[m - 1] += problem->data_prev[m - 1];
    }

    // upper border
    if (f_up) {
        // case case i = n - 1;
        for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
            gn = problem->data_prev[big_shift - m + j] - problem->data_prev[big_shift + j];
            ge = problem->data_prev[big_shift + j + 1] - problem->data_prev[big_shift + j];
            gw = problem->data_prev[big_shift + j - 1] - problem->data_prev[big_shift + j];
            if (problem->y_idx_bounds[1] < problem->y_n) {
                gs = problem->buf_ub[j] - problem->data_prev[big_shift + j];
            }
            else {
                gs = 0;
            }
            problem->data_cur[big_shift + j] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge + G(gw, K) * gw;
            problem->data_cur[big_shift + j] *= lambda;
            problem->data_cur[big_shift + j] += problem->data_prev[big_shift + j];
        }
        // j = 0
        gn = problem->data_prev[big_shift - m] - problem->data_prev[big_shift];
        ge = problem->data_prev[big_shift + 1] - problem->data_prev[big_shift];
        if (problem->y_idx_bounds[1] < problem->y_n) {
            gs = problem->buf_ub[0] - problem->data_prev[big_shift];
        }
        else {
            gs = 0;
        }
        problem->data_cur[big_shift] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
        problem->data_cur[big_shift] *= lambda;
        problem->data_cur[big_shift] += problem->data_prev[big_shift];

        // j = m - 1
        gn = problem->data_prev[big_shift - 1] - problem->data_prev[big_shift + m - 1];
        gw = problem->data_prev[big_shift + m - 2] - problem->data_prev[big_shift + m - 1];
        if (problem->y_idx_bounds[1] < problem->y_n) {
            gs = problem->buf_ub[m - 1] - problem->data_prev[big_shift];
        }
        else {
            gs = 0;
        }
        problem->data_cur[big_shift + m - 1] = G(gn, K) * gn + G(gs, K) * gs + G(gw, K) * gw;
        problem->data_cur[big_shift + m - 1] *= lambda;
        problem->data_cur[big_shift + m - 1] += problem->data_prev[big_shift + m - 1];
    }
}

void SolveProblem(problem_t* problem, int iter_n, double lambda, double K) {
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    const int m = problem->x_n;
    double *tmp = NULL;
    int inner_bounds = 0;
    MPI_Status status = {0};
    MPI_Request req_arr[2] = {0};
    int rc = 0;
    if (iter_n < 1) {
        return;
    }
#ifdef _DEBUG
    printf("[%i] started solving a problem\n", problem->proc_id);
    fflush(stdout);
#endif
    if (problem->buf_lb) {
        inner_bounds++;
    }
    if (problem->buf_ub) {
        inner_bounds++;
    }
#ifdef _DEBUG
    printf("[%i] got %i inner bounds\n", problem->proc_id, inner_bounds);
    fflush(stdout);
#endif
    tmp = problem->data_prev;
    problem->data_prev = problem->data_cur;
    problem->data_cur = tmp;
    FillInside(problem->data_prev, problem->data_cur, layer_width, problem->x_n, lambda, K);
    FillBounds(problem, lambda, K, 1, 1, 1, 1);
    if (iter_n > 1) {
        if (problem->y_idx_bounds[0] > 0) {
            MPI_Bsend(problem->data_cur, m, MPI_DOUBLE, problem->proc_id - 1, TAG_UP_BND, MPI_COMM_WORLD);
#ifdef _DEBUG
            printf("[%i] sent his low bound to proc %i\n", problem->proc_id, problem->proc_id - 1);
            fflush(stdout);
#endif
        }
        if (problem->y_idx_bounds[1] < problem->y_n) {
            MPI_Bsend(problem->data_cur + (layer_width - 1) * m, m, MPI_DOUBLE, problem->proc_id + 1, TAG_LOW_BND,
                      MPI_COMM_WORLD);
#ifdef _DEBUG
            printf("[%i] sent his up bound to proc %i\n", problem->proc_id, problem->proc_id + 1);
            fflush(stdout);
#endif
        }    
    }
    for (int t = 1; t < iter_n; t++) {
        tmp = problem->data_prev;
        problem->data_prev = problem->data_cur;
        problem->data_cur = tmp;
        FillInside(problem->data_prev, problem->data_cur, layer_width, problem->x_n, lambda, K);
        FillBounds(problem, lambda, K, 1, 1, 0, 0);

        if (problem->buf_lb) {
            MPI_Irecv(problem->buf_lb, m, MPI_DOUBLE, problem->proc_id - 1, TAG_LOW_BND,
                      MPI_COMM_WORLD, req_arr);
        }
        else {
            req_arr[0] = MPI_REQUEST_NULL;
        }
        if (problem->buf_ub) {
            MPI_Irecv(problem->buf_ub, m, MPI_DOUBLE, problem->proc_id + 1, TAG_UP_BND,
                      MPI_COMM_WORLD, req_arr + 1);
        }
        else {
            req_arr[1] = MPI_REQUEST_NULL;
        }
        for (int bnd_idx = 0; bnd_idx < inner_bounds; bnd_idx++) {
            int req_idx = 0;
            rc = MPI_Waitany(2, req_arr, &req_idx, &status);
            if (rc != MPI_SUCCESS) {
                printf("[%i] Error in wait, code %i\n", problem->proc_id, rc);
                fflush(stdout);
                exit(1);
            }
            if (status.MPI_TAG == TAG_LOW_BND) {
#ifdef _DEBUG
                printf("[%i] received his low bound from proc %i\n", problem->proc_id,
                       status.MPI_SOURCE);
                fflush(stdout);
#endif
                FillBounds(problem, lambda, K, 0, 0, 1, 0);
                if (t < iter_n - 1) {
                    MPI_Bsend(problem->data_cur, m, MPI_DOUBLE, problem->proc_id - 1, TAG_UP_BND,
                              MPI_COMM_WORLD);
#ifdef _DEBUG
                    printf("[%i] sent his low bound to proc %i\n", problem->proc_id,
                           problem->proc_id - 1);
                    fflush(stdout);
#endif
                }
            }
            else if (status.MPI_TAG == TAG_UP_BND) {
#ifdef _DEBUG
                printf("[%i] received his up bound from proc %i\n", problem->proc_id,
                       status.MPI_SOURCE);
                fflush(stdout);
#endif
                FillBounds(problem, lambda, K, 0, 0, 0, 1);
                if (t < iter_n - 1) {
                    MPI_Bsend(problem->data_cur + (layer_width - 1) * m, m, MPI_DOUBLE, problem->proc_id + 1, TAG_LOW_BND,
                              MPI_COMM_WORLD);
#ifdef _DEBUG
                    printf("[%i] sent his up bound to proc %i\n", problem->proc_id,
                           problem->proc_id + 1);
                    fflush(stdout);
#endif
                }
            }
            else {
                printf(
                    "[%i] Unknown message with tag %i was received from "
                    "proc %i!\n",
                    problem->proc_id, status.MPI_TAG, status.MPI_SOURCE);
                fflush(stdout);
            }
        }
    }
}

void KillProblem(problem_t* problem) {
    if (!problem) {
        return;
    }
    int size = 0;
    int rc = 0;
    if (problem->mpi_buffer) {
        MPI_Buffer_detach(problem->mpi_buffer, &size);
        if (rc != MPI_SUCCESS) {
            printf("[%i] Buffer detaching error! Code %i\n", problem->proc_id, rc);
        }
        free(problem->mpi_buffer);
    }
    free(problem->data_cur);
    free(problem->data_prev);
    free(problem->buf_lb);
    free(problem->buf_ub);
    free(problem);
}