#include <Windows.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "problem.h"
#include "image.h"

#define IMG_MSG_TAG 42
#define LENGTH_MSG_TAG 43

problem_t* share_image(image_t* img, int proc_id, int procs_n) {
    int w = 0, h = 0;
    double* data = NULL;
    if (img) {
        w = img->w;
        h = img->h;
        data = img->data;
    }
    MPI_Bcast(&w, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&h, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (proc_id != 0) {
        data = malloc(w * h * sizeof(double));
        if (!data) {
            printf("ERROR: allocation error");
            exit(1);
        }
    }
    MPI_Bcast(data, w * h, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    problem_t* problem = InitProblem(data, w, h, proc_id, procs_n);
    if (img) {
        delete_image(img);
    }
    else {
        free(data);
    }
    return problem;
}

image_t* gather_problems(problem_t* problem, int procs_n) {
    int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    double* data = NULL;
    int written = 0;
    MPI_Status status = {0};
    image_t* img = NULL;
    if (problem->proc_id == 0) {
        data = malloc(problem->x_n * problem->y_n * sizeof(double));
        if (!data) {
            printf("ERROR: allocation error");
            exit(1);
        }
        memcpy(data, problem->data_cur, problem->x_n * layer_width * sizeof(double));
        written += problem->x_n * layer_width;
        for (int i = 1; i < procs_n; i++) {
            MPI_Recv(&layer_width, 1, MPI_INT, i, LENGTH_MSG_TAG, MPI_COMM_WORLD, &status);
            MPI_Recv(data + written, problem->x_n * layer_width, MPI_DOUBLE, i, IMG_MSG_TAG, MPI_COMM_WORLD, &status);
            written += problem->x_n * layer_width;
        }
        img = malloc(sizeof(image_t));
        if (!img) {
            printf("ERROR: allocation error");
            exit(1);
        }
        img->data = data;
        img->w = problem->x_n;
        img->h = problem->y_n;
    }
    else {
        MPI_Send(&layer_width, 1, MPI_INT, 0, LENGTH_MSG_TAG, MPI_COMM_WORLD);
        MPI_Ssend(problem->data_cur, problem->x_n * layer_width, MPI_DOUBLE, 0, IMG_MSG_TAG, MPI_COMM_WORLD);
    }
    return img;
}

int main(int argc, char* argv[]) {
    int my_id, procs_n;
    double lambda = 1.0 / 6;
    double K = 0.1;
    int iter_n = 0;
    char* input_path = NULL;
    char* output_path = NULL;
    char* time_path = NULL;
    double sol_start_time = 0;
    double sol_elapsed_time = 0;
    double max_time = 0;
    problem_t* problem = NULL;
    image_t* img = NULL;
    FILE* desc = NULL;
    MPI_Init(&argc, &argv);                   // starts MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);    // get current process id
    MPI_Comm_size(MPI_COMM_WORLD, &procs_n);  // get number of processeser
#ifdef _DEBUG
    printf("[%i] MPI Initialized\n", my_id);
    fflush(stdout);
#endif
    if (argc < 4) {
        printf("ERROR: there must be 3 to 6 arguments: path in, path out, iterations number, lambda, K, time path");
        MPI_Finalize();
        exit(1);
    }
    input_path = argv[1];
    output_path = argv[2];
    iter_n = atoi(argv[3]);
    if (argc >= 5) {
        lambda = atof(argv[4]);
    }
    if (argc >= 6) {
        K = atof(argv[5]);
    }
    if (argc >= 7) {
        time_path = argv[6];
    }
    if (my_id == 0) {
        img = read_image(input_path);
    }
    problem = share_image(img, my_id, procs_n);
    if (!problem) {
        KillProblem(problem);
        exit(1);
    }
#ifdef _DEBUG
    printf("[%i] Problem is created\n", my_id);
    printf("[%i] Low idx %i, up idx %i, width %i\n", my_id, problem->y_idx_bounds[0],
           problem->y_idx_bounds[1], problem->y_idx_bounds[1] - problem->y_idx_bounds[0]);
    fflush(stdout);
#endif
    sol_start_time = MPI_Wtime();
    SolveProblem(problem, iter_n, lambda, K);
    sol_elapsed_time = MPI_Wtime() - sol_start_time;
#ifdef _DEBUG
    printf("[%i] Problem is solved in %.4lf s.\n", my_id, sol_elapsed_time);
    fflush(stdout);
#endif
    img = gather_problems(problem, procs_n);
    KillProblem(problem);
    MPI_Reduce(&sol_elapsed_time, &max_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    if (my_id == 0) {
        printf("Problem is solved in %.4lf s.\n", max_time);
        if (time_path) {
            desc = fopen(time_path, "a");
            fprintf(desc, "%lf\n", max_time);
            fclose(desc);
        }
        save_image(output_path, img);
        delete_image(img);
    }
    MPI_Finalize();
    return 0;
}