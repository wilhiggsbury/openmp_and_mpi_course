#include "problem.h"

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// up and low y bounds according to the index
// for the recipient
#define TAG_UP_BND 16
#define TAG_LOW_BND 32

problem_t* InitProblem(double* v0, int x_len, int y_len) {
    problem_t* problem = calloc(1, sizeof(problem_t));
    int proc_y_n = 0;
    if (!problem) {
        printf("Memory allocation error!\n");
        free(problem);
        return NULL;
    }
    problem->x_n = x_len;
    problem->y_n = y_len;
    problem->data_cur = malloc(x_len * y_len * sizeof(double));
    problem->data_prev = malloc(x_len * y_len * sizeof(double));
    if (!problem->data_cur || !problem->data_prev) {
        free(problem->data_cur);
        free(problem->data_prev);
        free(problem);
        return NULL;
    }
    memcpy(problem->data_cur, v0, x_len * y_len * sizeof(double));
    return problem;
}

double G(double x, double K) {
    return exp(-pow(fabs(x) / K, 2));
}

void FillInside(double* p, double* c, int n, int m, double lambda, double K) {
    double gn, gs, ge, gw;
    int i, j;
// warning C4849: collapse is ignored 
#pragma omp parallel for private(i, j, gn, gs, ge, gw) collapse(2)
    for (i = 1; i < n - 1; i++) {
        for (j = 1; j < m - 1; j++) {
            gn = p[(i - 1) * m + j] - p[i * m + j];
            gs = p[(i + 1) * m + j] - p[i * m + j];
            ge = p[i * m + j + 1] - p[i * m + j];
            gw = p[i * m + j - 1] - p[i * m + j];
            c[i * m + j] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge + G(gw, K) * gw;
            c[i * m + j] *= lambda;
            c[i * m + j] += p[i * m + j];
        }
    }
}

void FillBounds(problem_t* problem, double lambda, double K, int f_left, int f_right, int f_low, int f_up) {
    double gn, gs, ge, gw;
    const int layer_width = problem->y_n;
    int m = problem->x_n;
    int i, j;
    const int big_shift = (layer_width - 1) * m;

    if (f_left) {
        // left border
#pragma omp parallel for private(i, gn, gs, ge)
        for (i = 1; i < layer_width - 1; i++) {  // j is for x, i is for y
            gs = problem->data_prev[(i + 1) * m] - problem->data_prev[i * m];
            ge = problem->data_prev[i * m + 1] - problem->data_prev[i * m];
            gn = problem->data_prev[(i - 1) * m + 1] - problem->data_prev[i * m];
            problem->data_cur[i * m] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
            problem->data_cur[i * m] *= lambda;
            problem->data_cur[i * m] += problem->data_prev[i * m];
        }
    }

    if (f_right) {
        // right border
#pragma omp parallel for private(i, gn, gs, gw)
        for (i = 1; i < layer_width - 1; i++) {  // j is for x, i is for y
            gs = problem->data_prev[(i + 2) * m - 1] - problem->data_prev[(i + 1) * m - 1];
            gw = problem->data_prev[(i + 1) * m - 2] - problem->data_prev[(i + 1) * m - 1];
            gn = problem->data_prev[(i - 1) * m + 1] - problem->data_prev[(i + 1) * m - 1];
            problem->data_cur[(i + 1) * m - 1] = G(gn, K) * gn + G(gs, K) * gs + G(ge, K) * ge;
            problem->data_cur[(i + 1) * m - 1] *= lambda;
            problem->data_cur[(i + 1) * m - 1] += problem->data_prev[(i + 1) * m - 1];
        }
    }

    // lower border
    if (f_low) {
        // case i = 0;
#pragma omp parallel for private(j, gs, ge, gw)
        for (j = 1; j < m - 1; j++) {  // j is for x, i is for y
            gs = problem->data_prev[m + j] - problem->data_prev[j];
            ge = problem->data_prev[j + 1] - problem->data_prev[j];
            gw = problem->data_prev[j - 1] - problem->data_prev[j];
            problem->data_cur[j] = G(gs, K) * gs + G(ge, K) * ge + G(gw, K) * gw;
            problem->data_cur[j] *= lambda;
            problem->data_cur[j] += problem->data_prev[j];
        }
        // j = 0
        gs = problem->data_prev[m] - problem->data_prev[0];
        ge = problem->data_prev[1] - problem->data_prev[0];
        problem->data_cur[0] = G(gs, K) * gs + G(ge, K) * ge;
        problem->data_cur[0] *= lambda;
        problem->data_cur[0] += problem->data_prev[0];

        // j = m - 1
        gs = problem->data_prev[2 * m - 1] - problem->data_prev[m - 1];
        gw = problem->data_prev[m - 2] - problem->data_prev[m - 1];
        problem->data_cur[m - 1] = G(gs, K) * gs + G(gw, K) * gw;
        problem->data_cur[m - 1] *= lambda;
        problem->data_cur[m - 1] += problem->data_prev[m - 1];
    }

    // upper border
    if (f_up) {
        // case case i = n - 1;
#pragma omp parallel for private(j, gn, ge, gw)
        for (j = 1; j < m - 1; j++) {  // j is for x, i is for y
            gn = problem->data_prev[big_shift - m + j] - problem->data_prev[big_shift + j];
            ge = problem->data_prev[big_shift + j + 1] - problem->data_prev[big_shift + j];
            gw = problem->data_prev[big_shift + j - 1] - problem->data_prev[big_shift + j];
            problem->data_cur[big_shift + j] = G(gn, K) * gn + G(ge, K) * ge + G(gw, K) * gw;
            problem->data_cur[big_shift + j] *= lambda;
            problem->data_cur[big_shift + j] += problem->data_prev[big_shift + j];
        }
        // j = 0
        gn = problem->data_prev[big_shift - m] - problem->data_prev[big_shift];
        ge = problem->data_prev[big_shift + 1] - problem->data_prev[big_shift];
        problem->data_cur[big_shift] = G(gn, K) * gn + G(ge, K) * ge;
        problem->data_cur[big_shift] *= lambda;
        problem->data_cur[big_shift] += problem->data_prev[big_shift];

        // j = m - 1
        gn = problem->data_prev[big_shift - 1] - problem->data_prev[big_shift + m - 1];
        gw = problem->data_prev[big_shift + m - 2] - problem->data_prev[big_shift + m - 1];
        problem->data_cur[big_shift + m - 1] = G(gn, K) * gn + G(gw, K) * gw;
        problem->data_cur[big_shift + m - 1] *= lambda;
        problem->data_cur[big_shift + m - 1] += problem->data_prev[big_shift + m - 1];
    }
}

void SolveProblem(problem_t* problem, int iter_n, double lambda, double K) {
    const int m = problem->x_n;
    double *tmp = NULL;
    if (iter_n < 1) {
        return;
    }
    tmp = problem->data_prev;
    problem->data_prev = problem->data_cur;
    problem->data_cur = tmp;
    FillInside(problem->data_prev, problem->data_cur, problem->y_n, problem->x_n, lambda, K);
    FillBounds(problem, lambda, K, 1, 1, 1, 1);
    for (int t = 1; t < iter_n; t++) {
        tmp = problem->data_prev;
        problem->data_prev = problem->data_cur;
        problem->data_cur = tmp;
        FillInside(problem->data_prev, problem->data_cur, problem->y_n, problem->x_n, lambda, K);
        FillBounds(problem, lambda, K, 1, 1, 0, 0);
    }
}

void KillProblem(problem_t* problem) {
    if (!problem) {
        return;
    }
    free(problem->data_cur);
    free(problem->data_prev);
    free(problem);
}