#include <Windows.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#include "problem.h"
#include "image.h"

#define IMG_MSG_TAG 42
#define LENGTH_MSG_TAG 43

problem_t* image2problem(image_t* img) {
    problem_t* problem = InitProblem(img->data, img->w, img->h);
    delete_image(img);
    return problem;
}

image_t* problem2image(problem_t* problem) {
    image_t* img = malloc(sizeof(image_t));
    double* data = malloc(problem->x_n * problem->y_n * sizeof(double));
    if (!img || !data) {
        printf("Allcation error\n");
        free(img);
        free(data);
        return NULL;
    }
    memcpy(data, problem->data_cur, problem->x_n * problem->y_n * sizeof(double));
    img->data = data;
    img->w = problem->x_n;
    img->h = problem->y_n;
    KillProblem(problem);
    return img;
}

int main(int argc, char* argv[]) {
    double lambda = 1.0 / 6;
    double K = 0.1;
    int iter_n = 0;
    int threads_num = 0;
    char* input_path = NULL;
    char* output_path = NULL;
    char* time_path = NULL;
    double sol_start_time = 0;
    double sol_elapsed_time = 0;
    double max_time = 0;
    problem_t* problem = NULL;
    image_t* img = NULL;
    FILE* desc = NULL;
    if (argc < 4) {
        printf("ERROR: there must be 4 to 7 arguments: path in, path out, threads number, iterations number, lambda, K, time path");
        return 1;
    }
    input_path = argv[1];
    output_path = argv[2];
    threads_num = atoi(argv[3]);
    iter_n = atoi(argv[4]);
    if (argc >= 6) {
        lambda = atof(argv[5]);
    }
    if (argc >= 7) {
        K = atof(argv[6]);
    }
    if (argc >= 8) {
        time_path = argv[7];
    }
    omp_set_num_threads(threads_num);
    img = read_image(input_path);
    problem = image2problem(img);
    if (!problem) {
        KillProblem(problem);
        return 0;
    }
    sol_start_time = omp_get_wtime();
    SolveProblem(problem, iter_n, lambda, K);
    sol_elapsed_time = omp_get_wtime() - sol_start_time;
    printf("Problem is solved in %.4lf s.\n", sol_elapsed_time);
    if (time_path) {
        desc = fopen(time_path, "a");
        fprintf(desc, "%lf\n", sol_elapsed_time);
        fclose(desc);
    }
    img = problem2image(problem);
    save_image(output_path, img);
    delete_image(img);
    return 0;
}