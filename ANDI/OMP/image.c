#include <stdio.h>
#include <stdlib.h>
#include "image.h"

#define BUF_SIZE 100

image_t* read_image(const char* path) {
    if (!path) {
        return NULL;
    }
    FILE* desc = fopen(path, "rb");
    if (!desc) {
        printf("ERROR: Unable to open %s", path);
        return NULL;
    }
    char buf[BUF_SIZE] = {0};
    char c = getc(desc);
    int i = 0;
    while (c != EOF && c != '\n' && i < BUF_SIZE - 1) {
        buf[i++] = c;
        c = getc(desc);
    }
    if (i == BUF_SIZE - 1) {
        return NULL;
    }
    int w = 0, h = 0;
    int filled = sscanf(buf, "%i %i", &w, &h);
    if (filled != 2 || w <= 0 || h <= 0) {
        return NULL;
    }
    int img_size = w * h;
    double* arr = malloc(img_size * sizeof(double));
    unsigned char* big_buf = malloc(img_size);
    image_t* img = malloc(sizeof(image_t));
    if (!arr || !big_buf || !img) {
        free(arr);
        free(big_buf);
        free(img);
        fclose(desc);
        return NULL;
    }
    int read = fread(big_buf, 1, img_size, desc);
    if (read != img_size) {
        printf("WARNING: fread returned less bytes than image size was declared\n");
    }
    for (int i = 0; i < img_size; i++) {
        arr[i] = big_buf[i] / 255.0;
    }
    free(big_buf);
    img->data = arr;
    img->w = w;
    img->h = h;
    fclose(desc);
    return img;
}

void save_image(const char* path, image_t* img) {
    if (!img || !path) {
        return;
    }
    FILE* desc = fopen(path, "wb");
    if (!desc) {
        printf("ERROR: Unable to open %s", path);
        return;
    }
    char buf[BUF_SIZE] = {0};
    int written = sprintf_s(buf, BUF_SIZE, "%i %i\n", img->w, img->h);
    if (!buf[0]) {
        printf("ERROR: Unable to write image size");
        fclose(desc);
        return;
    }
    fwrite(buf, 1, written, desc);
    for (int i = 0; i < img->w * img->h; i++) {
        int pixel = (int)(img->data[i] * 255);
        if (pixel < 0) {
            pixel = 0;
        }
        putc(pixel, desc);
    }
    fclose(desc);
}

void delete_image(image_t* image) {
    if (!image) {
        return;
    }
    free(image->data);
    free(image);
}