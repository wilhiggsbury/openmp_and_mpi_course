#pragma once

typedef struct {
    double* data;
    int w;
    int h;
} image_t;


image_t* read_image(const char* path);

void save_image(const char* path, image_t* img);

void delete_image(image_t* image);