#pragma once

typedef struct {
    double* data_prev;
    double* data_cur;
    int y_n;
    int x_n;
} problem_t;

problem_t* InitProblem(double* v0, int x_len, int y_len);

void SolveProblem(problem_t* problem, int iter_n, double lambda, double K);

void KillProblem(problem_t* problem);
