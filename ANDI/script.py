import subprocess

procs_n_list = [1, 2, 4, 6, 8, 10, 12]
iters = 10
print("--- MPI ---")
for procs_n in procs_n_list:
    for _ in range(iters):
        subprocess.run(["mpiexec", "-n", str(procs_n), "MPI\\build\\Release\\mpi_lab.exe", 
                        "img\\canaletto.txt", " ", "100", "0.2", "0.03", f"matlab\\time_{procs_n}_mpi.txt"])
        print()
print("--- OMP ---")
for procs_n in procs_n_list:
    for _ in range(iters):
        subprocess.run(["OMP\\build\\Release\\omp_lab.exe", "img\\canaletto.txt", 
                        " ", str(procs_n), "100", "0.2", "0.03", f"matlab\\time_{procs_n}_omp.txt"])
        print()
