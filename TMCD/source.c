#include <Windows.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "problem.h"

double* Fillv0(double inside_value, double bound_value, double inside_peak, int n, int m) {
    double* result = malloc(n * m * sizeof(double));
    int peak_n_rad = n / 10;
    int peak_m_rad = m / 10;
    for (int j = 0; j < m; j++) {
        result[j] = bound_value;
    }
    for (int i = 1; i < n - 1; i++) {
        result[i * m] = bound_value;
        for (int j = 1; j < m - 1; j++) {
            if (abs(i - n / 2) < peak_n_rad || abs(j - m / 2) < peak_m_rad) {
                result[i * m + j] = inside_peak;
            }
            else {
                result[i * m + j] = inside_value;
            }
        }
        result[(i + 1) * m - 1] = bound_value;
    }
    for (int j = 0; j < m; j++) {
        result[(n - 1) * m + j] = bound_value;
    }
    return result;
}

int main(int argc, char* argv[]) {
    int my_id, procs_n;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int x_n = 256;
    int y_n = 256;
    double end_time = 10.00;
    int time_n = 10000;
    int print_times = 0;
    double a = 0.05;
    double T_bound = 10;
    double x_int[] = {0, 1};
    double y_int[] = {0, 1};
    double* v0 = NULL;
    char* solution_path = NULL;
    char* time_path = NULL;
    double sol_start_time = 0;
    double sol_elapsed_time = 0;
    double max_time = 0;
    problem_t* problem = NULL;
    FILE* desc = NULL;
    MPI_Init(&argc, &argv);                   // starts MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &my_id);    // get current process id
    MPI_Comm_size(MPI_COMM_WORLD, &procs_n);  // get number of processeser
    if (argc > 1) {
        solution_path = argv[1];
    }
    if (argc > 2) {
        time_path = argv[2];
    }
#ifdef _DEBUG
    printf("[%i] MPI Initialized\n", my_id);
    fflush(stdout);
#endif
    v0 = Fillv0(0, T_bound, T_bound / 2, x_n, y_n);
    problem = InitProblem(v0, time_n, end_time, a, T_bound, x_int[0], x_int[1], y_int[0], y_int[1],
                          x_n, y_n, my_id, procs_n);
    free(v0);
    if (!problem) {
        KillProblem(problem);
        exit(1);
    }
#ifdef _DEBUG
    printf("[%i] Problem is created, t num is %i\n", my_id, problem->t_num);
    printf("[%i] Low idx %i, up idx %i, width %i\n", my_id, problem->y_idx_bounds[0],
           problem->y_idx_bounds[1], problem->y_idx_bounds[1] - problem->y_idx_bounds[0]);
    fflush(stdout);
#endif
    sol_start_time = MPI_Wtime();
    SolveProblem(problem);
    sol_elapsed_time = MPI_Wtime() - sol_start_time;
#ifdef _DEBUG
    printf("[%i] Problem is solved\n", my_id);
    fflush(stdout);
#endif
    if (print_times > 0) {
        PrintSolution(problem, print_times, solution_path);
    }
    KillProblem(problem);
    MPI_Reduce(&sol_elapsed_time, &max_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    if (my_id == 0) {
        if (time_path) {
            desc = fopen(time_path, "a");
            fprintf(desc, "%lf\n", max_time);
            fclose(desc);
        }
        printf("Computational time: %lf s.", max_time);
    }
    MPI_Finalize();
    return 0;
}