import subprocess

procs_n_list = [1, 2, 4, 6, 8, 10, 12]
iters = 20
for procs_n in procs_n_list:
    for _ in range(iters):
        subprocess.run(["mpiexec", "-n", str(procs_n), "build\\Release\\mpi_lab.exe", "matlab", f"matlab\\time_{procs_n}.txt"])
        print()
