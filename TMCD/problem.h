#pragma once

typedef struct {
    double** v;  // first axis is time
    double* buf_lb;
    double* buf_ub;
    double* mpi_buffer;
    int t_num;
    int x_n;
    double h_x;
    double a_x;
    int y_n;
    double h_y;
    double a_y;
    double dt;
    double a;
    double bound_val;
    int proc_id;
    int y_idx_bounds[2];
} problem_t;

problem_t* InitProblem(double* v0, int time_len, double end_time, double a_coef, double bound_val,
                       double a_x, double b_x, double a_y, double b_y, int x_len, int y_len,
                       int proc_id, int procs_n);

void SolveProblem(problem_t* problem);

void PrintSolution(problem_t* problem, int time_n, const char* folder_path);

void KillProblem(problem_t* problem);
