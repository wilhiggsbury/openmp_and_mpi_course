clear all
close all
x = readmatrix("x_grid.txt");
y = readmatrix("y_grid.txt");
y = flip(y);
[X, Y] = meshgrid(x, y);
v = readmatrix("grid.txt");
time = readmatrix("time.txt");
V = zeros([size(time, 2) size(v, 2) size(v, 2)]);
for i=1:size(time, 2)
    V(i, :, :) = v((i - 1) * size(V, 2) + 1 : i * size(V, 2), :);
end
for i=1:size(time, 2)
    figure
    contourf(X, Y, squeeze(V(i, :, :)))
    title(sprintf('T = %g', time(i)))
    xlabel('X')
    ylabel('Y')
    colormap hot
    colorbar()
end


