#include "problem.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// up and low y bounds according to the index
// for the recipient
#define TAG_UP_BND 16
#define TAG_LOW_BND 32
#define TAG_V_MSG 42

void get_y_bounds(int* bounds, int cur_proc_id, int procs_n, int y_n) {
    int layer_thick = y_n / procs_n;
    bounds[0] = cur_proc_id * layer_thick;
    bounds[1] = bounds[0] + layer_thick;
    if (cur_proc_id == procs_n - 1) {
        bounds[1] += y_n % procs_n;
    }
}

problem_t* InitProblem(double* v0, int time_len, double end_time, double a_coef, double bound_val,
                       double a_x, double b_x, double a_y, double b_y, int x_len, int y_len,
                       int proc_id, int procs_n) {
    problem_t* problem = calloc(1, sizeof(problem_t));
    double** v_grid = malloc(time_len * sizeof(double*));
    int proc_y_n = 0;
    if (!problem || !v_grid) {
        printf("Memory allocation error!\n");
        free(problem);
        free(v_grid);
        return NULL;
    }
    problem->proc_id = proc_id;
    get_y_bounds(problem->y_idx_bounds, proc_id, procs_n, y_len);
    if (problem->y_idx_bounds[0] != 0) {
        problem->buf_lb = malloc(x_len * sizeof(double));
        if (!problem->buf_lb) {
            printf("Memory allocation error!\n");
            free(problem);
            free(v_grid);
            return NULL;
        }
        memcpy(problem->buf_lb, v0 + (problem->y_idx_bounds[0] - 1) * x_len,
               x_len * sizeof(double));
    }
    if (problem->y_idx_bounds[1] != y_len) {
        problem->buf_ub = malloc(x_len * sizeof(double));
        if (!problem->buf_ub) {
            printf("Memory allocation error!\n");
            free(problem->buf_lb);
            free(problem);
            free(v_grid);
            return NULL;
        }
        memcpy(problem->buf_ub, v0 + problem->y_idx_bounds[1] * x_len, x_len * sizeof(double));
    }
    proc_y_n = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    problem->x_n = x_len;
    problem->y_n = y_len;
    problem->h_x = (b_x - a_x) / (x_len - 1);
    problem->h_y = (b_y - a_y) / (y_len - 1);
    problem->a_x = a_x;
    problem->a_y = a_y;
    int buf_num = !!problem->buf_lb + !!problem->buf_ub;
    if (buf_num > 0) {
        problem->mpi_buffer = malloc(buf_num * x_len * sizeof(double) + 200);
        if (!problem->mpi_buffer) {
            printf("Memory allocation error!\n");
            free(problem->buf_lb);
            free(problem->buf_ub);
            free(problem);
            free(v_grid);
            return NULL;
        }
        int rc = MPI_Buffer_attach(problem->mpi_buffer, buf_num * x_len * sizeof(double) + 200);
        if (rc != MPI_SUCCESS) {
            printf("Buffer attaching error! %i\n", rc);
            free(problem->buf_lb);
            free(problem->buf_ub);
            free(problem);
            free(v_grid);
            return NULL;
        }
    }
    for (int i = 0; i < time_len; i++) {
        v_grid[i] = malloc(x_len * proc_y_n * sizeof(double));
        if (!v_grid[i]) {
            printf("Memory allocation error!\n");
            for (int j = i - 1; j >= 0; j--) {
                free(v_grid[j]);
            }
            free(problem);
            free(v_grid);
            return NULL;
        }
    }
    memcpy(v_grid[0], v0 + problem->y_idx_bounds[0] * x_len, x_len * proc_y_n * sizeof(double));
    problem->v = v_grid;
    problem->dt = end_time / (time_len - 1);
    problem->t_num = time_len;
    problem->a = a_coef;
    problem->bound_val = bound_val;
    double check = 2 * a_coef * a_coef *
                   (1.0 / (problem->h_x * problem->h_x) + 1.0 / (problem->h_y * problem->h_y));
    check = 1.0 / check;
    if (problem->dt >= check) {
        printf("WARNING: the time step must be smaller than %lf but it is %lf!\n", check,
               problem->dt);
    }
    return problem;
}

void FillBoundCond(problem_t* problem, int cur_t_idx) {
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    for (int i = 0; i < layer_width; i++) {
        problem->v[cur_t_idx][i * problem->x_n] = problem->bound_val;
        problem->v[cur_t_idx][(i + 1) * problem->x_n - 1] = problem->bound_val;
        if (i == 0 && problem->y_idx_bounds[0] == 0 ||
            i == layer_width - 1 && problem->y_idx_bounds[1] == problem->y_n) {
            for (int j = 1; j < problem->x_n - 1; j++) {
                problem->v[cur_t_idx][i * problem->x_n + j] = problem->bound_val;
            }
        }
    }
}

void FillInside(problem_t* problem, int cur_t_idx, const double ae, const double an,
                const double ap) {
    const double aw = ae, as = an;
    double* v_cur = problem->v[cur_t_idx];
    double* v_prev = problem->v[cur_t_idx - 1];
    const int m = problem->x_n;
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    for (int i = 1; i < layer_width - 1; i++) {
        for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
            v_cur[i * m + j] = (1 - ap) * v_prev[i * m + j] + ae * v_prev[i * m + j + 1] +
                               aw * v_prev[i * m + j - 1] + an * v_prev[(i + 1) * m + j] +
                               as * v_prev[(i - 1) * m + j];
        }
    }
}

void SolveProblem(problem_t* problem) {
    double ae, an, ap, aw, as;
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    int i;
    const int m = problem->x_n;
    double *v_cur = NULL, *v_prev = NULL;
    int inner_bounds = 0;
    MPI_Status status = {0};
    MPI_Request req_arr[2] = {0};
    int rc = 0;
    if (problem->t_num < 2) {
        return;
    }
    ae = problem->a * problem->a * problem->dt / (problem->h_x * problem->h_x);
    an = problem->a * problem->a * problem->dt / (problem->h_y * problem->h_y);
    aw = ae;
    as = an;
    ap = ae + aw + an + as;
#ifdef _DEBUG
    printf("[%i] started solving a problem\n", problem->proc_id);
    fflush(stdout);
#endif
    if (problem->buf_lb) {
        inner_bounds++;
    }
    if (problem->buf_ub) {
        inner_bounds++;
    }
#ifdef _DEBUG
    printf("[%i] got %i inner bounds\n", problem->proc_id, inner_bounds);
    fflush(stdout);
#endif
    FillBoundCond(problem, 1);
    FillInside(problem, 1, ae, an, ap);
    v_cur = problem->v[1];
    v_prev = problem->v[0];
    if (problem->y_idx_bounds[0] > 0) {
        // i = 0;
        for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
            v_cur[j] = (1 - ap) * v_prev[j] + ae * v_prev[j + 1] + aw * v_prev[j - 1] +
                       an * v_prev[m + j] + as * problem->buf_lb[j];
        }
        if (problem->t_num > 2) {
            MPI_Bsend(v_cur, m, MPI_DOUBLE, problem->proc_id - 1, TAG_UP_BND, MPI_COMM_WORLD);
#ifdef _DEBUG
            printf("[%i] sent his low bound to proc %i\n", problem->proc_id, problem->proc_id - 1);
            fflush(stdout);
#endif
        }
    }
    if (problem->y_idx_bounds[1] < problem->y_n) {
        i = layer_width - 1;
        for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
            v_cur[i * m + j] = (1 - ap) * v_prev[i * m + j] + ae * v_prev[i * m + j + 1] +
                               aw * v_prev[i * m + j - 1] + an * problem->buf_ub[j] +
                               as * v_prev[(i - 1) * m + j];
        }
        if (problem->t_num > 2) {
            MPI_Bsend(v_cur + i * m, m, MPI_DOUBLE, problem->proc_id + 1, TAG_LOW_BND,
                      MPI_COMM_WORLD);
#ifdef _DEBUG
            printf("[%i] sent his up bound to proc %i\n", problem->proc_id, problem->proc_id + 1);
            fflush(stdout);
#endif
        }
    }

    for (int t = 2; t < problem->t_num; t++) {
        FillBoundCond(problem, t);
        FillInside(problem, t, ae, an, ap);
        v_cur = problem->v[t];
        v_prev = problem->v[t - 1];

        if (problem->buf_lb) {
            MPI_Irecv(problem->buf_lb, m, MPI_DOUBLE, problem->proc_id - 1, TAG_LOW_BND,
                      MPI_COMM_WORLD, req_arr);
        }
        else {
            req_arr[0] = MPI_REQUEST_NULL;
        }
        if (problem->buf_ub) {
            MPI_Irecv(problem->buf_ub, m, MPI_DOUBLE, problem->proc_id + 1, TAG_UP_BND,
                      MPI_COMM_WORLD, req_arr + 1);
        }
        else {
            req_arr[1] = MPI_REQUEST_NULL;
        }
        for (int bnd_idx = 0; bnd_idx < inner_bounds; bnd_idx++) {
            int req_idx = 0;
            rc = MPI_Waitany(2, req_arr, &req_idx, &status);
            if (rc != MPI_SUCCESS) {
                printf("[%i] Error in wait, code %i\n", problem->proc_id, rc);
                fflush(stdout);
                exit(1);
            }
            if (status.MPI_TAG == TAG_LOW_BND) {
#ifdef _DEBUG
                printf("[%i] received his low bound from proc %i\n", problem->proc_id,
                       status.MPI_SOURCE);
                fflush(stdout);
#endif
                // i = 0;
                for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
                    v_cur[j] = (1 - ap) * v_prev[j] + ae * v_prev[j + 1] + aw * v_prev[j - 1] +
                               an * v_prev[m + j] + as * problem->buf_lb[j];
                }
                if (t < problem->t_num - 1) {
                    MPI_Bsend(v_cur, m, MPI_DOUBLE, problem->proc_id - 1, TAG_UP_BND,
                              MPI_COMM_WORLD);
#ifdef _DEBUG
                    printf("[%i] sent his low bound to proc %i\n", problem->proc_id,
                           problem->proc_id - 1);
                    fflush(stdout);
#endif
                }
            }
            else if (status.MPI_TAG == TAG_UP_BND) {
#ifdef _DEBUG
                printf("[%i] received his up bound from proc %i\n", problem->proc_id,
                       status.MPI_SOURCE);
                fflush(stdout);
#endif
                i = layer_width - 1;
                for (int j = 1; j < m - 1; j++) {  // j is for x, i is for y
                    v_cur[i * m + j] = (1 - ap) * v_prev[i * m + j] + ae * v_prev[i * m + j + 1] +
                                       aw * v_prev[i * m + j - 1] + an * problem->buf_ub[j] +
                                       as * v_prev[(i - 1) * m + j];
                }
                if (t < problem->t_num - 1) {
                    MPI_Bsend(v_cur + i * m, m, MPI_DOUBLE, problem->proc_id + 1, TAG_LOW_BND,
                              MPI_COMM_WORLD);
#ifdef _DEBUG
                    printf("[%i] sent his up bound to proc %i\n", problem->proc_id,
                           problem->proc_id + 1);
                    fflush(stdout);
#endif
                }
            }
            else {
                printf(
                    "[%i] Unknown message with tag %i was received from "
                    "proc %i!\n",
                    problem->proc_id, status.MPI_TAG, status.MPI_SOURCE);
                fflush(stdout);
            }
        }
    }
}

void Write1dGrid(double start, double delta, int n, FILE* file) {
    double val = start;
    fprintf(file, "%lf", val);
    for (int i = 1; i < n; i++) {
        val += delta;
        fprintf(file, ", %lf", val);
    }
}

void WritePart(double* buffer, int n, int m, FILE* file) {
    for (int j = 0; j < n; j++) {
        fprintf(file, "%lf", buffer[j * m]);
        for (int k = 1; k < m; k++) {
            fprintf(file, ", %lf", buffer[j * m + k]);
        }
        fprintf(file, "\n");
    }
}

void WriteV(problem_t* problem, int* times, int time_n, FILE* file) {
    double* buffer = malloc(2 * problem->x_n *
                            (problem->y_idx_bounds[1] - problem->y_idx_bounds[0]) * sizeof(double));
    int procs_n = 0;
    int elems = 0;
    MPI_Status status = {0};
    int rc = 0;
    if (!buffer) {
        printf("Memory allocation error!");
        exit(1);
    }
    MPI_Comm_size(MPI_COMM_WORLD, &procs_n);
#ifdef _DEBUG
    printf("[%i] Got %i procs in group\n", problem->proc_id, procs_n);
    fflush(stdout);
#endif
    for (int i = 0; i < time_n; i++) {
        WritePart(problem->v[times[i]], problem->y_idx_bounds[1] - problem->y_idx_bounds[0],
                  problem->x_n, file);
#ifdef _DEBUG
        printf("[%i] Written my part for t_idx %i\n", problem->proc_id, i);
        fflush(stdout);
#endif
        for (int j = 1; j < procs_n; j++) {
            MPI_Probe(j, TAG_V_MSG, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_DOUBLE, &elems);
#ifdef _DEBUG
            printf("[%i] Waiting for part from proc %i. Size %i\n", problem->proc_id, j, elems);
            fflush(stdout);
#endif
            rc = MPI_Recv(buffer, elems, MPI_DOUBLE, j, TAG_V_MSG, MPI_COMM_WORLD, &status);
            if (rc != MPI_SUCCESS) {
                printf("Error during print communication with proc %i! Code %i\n", j, rc);
                exit(1);
            }
            WritePart(buffer, elems / problem->x_n, problem->x_n, file);
        }
    }
    free(buffer);
}

void PrintSolution(problem_t* problem, int time_n, const char* folder_path) {
    const char x_grid_fname[] = "x_grid.txt";
    const char y_grid_fname[] = "y_grid.txt";
    const char sol_fname[] = "grid.txt";
    const char time_fname[] = "time.txt";
    int path_len = 0;
    char* buffer = NULL;
    int buffer_len = 0;
    int idx_to_write = 0;
    FILE* desc = NULL;
    const int layer_width = problem->y_idx_bounds[1] - problem->y_idx_bounds[0];
    int* times = NULL;
    int time_diff = problem->t_num / (time_n - 1);
    times = malloc(time_n * sizeof(int));
    if (!times) {
        if (!buffer) {
            printf("Memory allocation error!\n");
            return;
        }
    }
    times[0] = 0;
    times[time_n - 1] = problem->t_num - 1;
    for (int i = 1; i < time_n - 1; i++) {
        times[i] = times[i - 1] + time_diff;
    }
    if (problem->proc_id == 0) {
        if (folder_path) {
            path_len = strlen(folder_path);
        }
        buffer_len = sizeof(x_grid_fname);
        buffer_len = max(buffer_len, sizeof(y_grid_fname));
        buffer_len = max(buffer_len, sizeof(sol_fname));
        buffer_len = max(buffer_len, sizeof(time_fname));
        buffer_len += path_len + 2;
        buffer = malloc(buffer_len);
        if (!buffer) {
            printf("Memory allocation error!\n");
            return;
        }
        if (folder_path) {
            strcpy(buffer, folder_path);
        }
        idx_to_write = path_len;
        if (path_len > 0 &&
            (folder_path[path_len - 1] != '\\' || folder_path[path_len - 1] != '/')) {
            buffer[path_len] = '/';
            idx_to_write++;
        }
        strcpy(buffer + idx_to_write, x_grid_fname);
        desc = fopen(buffer, "w");
        if (!desc) {
            printf("File %s open error!\n", buffer);
            free(buffer);
            return;
        }
        Write1dGrid(problem->a_x, problem->h_x, problem->x_n, desc);
        fclose(desc);
        strcpy(buffer + idx_to_write, y_grid_fname);
        desc = fopen(buffer, "w");
        if (!desc) {
            printf("File %s open error!\n", buffer);
            free(buffer);
            return;
        }
        Write1dGrid(problem->a_y, problem->h_y, problem->y_n, desc);
        fclose(desc);
        strcpy(buffer + idx_to_write, sol_fname);
        desc = fopen(buffer, "w");
        if (!desc) {
            printf("File %s open error!\n", buffer);
            free(buffer);
            return;
        }
        WriteV(problem, times, time_n, desc);
        fclose(desc);
        strcpy(buffer + idx_to_write, time_fname);
        desc = fopen(buffer, "w");
        if (!desc) {
            printf("File %s open error!\n", buffer);
            free(buffer);
            return;
        }
        fprintf(desc, "%lf", 0.0);
        for (int i = 1; i < time_n; i++) {
            fprintf(desc, ", %lf", problem->dt * times[i]);
        }
        fclose(desc);
        free(buffer);
    }
    else {
        for (int i = 0; i < time_n; i++) {
            MPI_Ssend(problem->v[times[i]], layer_width * problem->x_n, MPI_DOUBLE, 0, TAG_V_MSG,
                      MPI_COMM_WORLD);
#ifdef _DEBUG
            printf("[%i] Sent %i elems to proc %i\n", problem->proc_id, layer_width * problem->x_n,
                   0);
            fflush(stdout);
#endif
        }
    }
    free(times);
}

void KillProblem(problem_t* problem) {
    if (!problem) {
        return;
    }
    int size = 0;
    int rc = 0;
    if (problem->mpi_buffer) {
        MPI_Buffer_detach(problem->mpi_buffer, &size);
        if (rc != MPI_SUCCESS) {
            printf("[%i] Buffer detaching error! Code %i\n", problem->proc_id, rc);
        }
        free(problem->mpi_buffer);
    }
    for (int i = 0; i < problem->t_num; i++) {
        free(problem->v[i]);
    }
    free(problem->buf_lb);
    free(problem->buf_ub);
    free(problem->v);
    free(problem);
}